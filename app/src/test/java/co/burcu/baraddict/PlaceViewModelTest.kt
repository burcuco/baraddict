package co.burcu.baraddict

import co.burcu.baraddict.model.Geometry
import co.burcu.baraddict.model.Location
import co.burcu.baraddict.model.Place
import co.burcu.baraddict.model.PlaceResponse
import co.burcu.baraddict.network.PlaceApi
import co.burcu.baraddict.ui.place.PlaceListFactoryViewModel
import co.burcu.baraddict.ui.place.PlaceListViewModel
import io.reactivex.Observable
import io.reactivex.observers.TestObserver
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnitRunner

/**
 * Created by Burcu Yalcinkaya on 08/10/2018.
 */

@RunWith(MockitoJUnitRunner::class)
class PlaceViewModelTest {

    private var placeListViewModel: PlaceListViewModel? = null

    @Mock
    private lateinit var placeApi: PlaceApi

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)

        val placeListFactoryViewModel = PlaceListFactoryViewModel("")
        placeListViewModel = placeListFactoryViewModel.create(PlaceListViewModel::class.java)
    }

    @Test
    fun testEmptyData() {
        val placeResponse = PlaceResponse(results = listOf())

        Mockito.`when`(placeApi.getPlaces("0,5", "bar", "distance", BuildConfig.API_KEY)).thenReturn(Observable.just(placeResponse))
        val result = placeApi.getPlaces("0,5", "bar", "distance", BuildConfig.API_KEY)

        val testObserver = TestObserver<PlaceResponse>()
        result.subscribe(testObserver)
        testObserver.assertComplete()
        testObserver.assertNoErrors()
        val listResult = testObserver.values()[0]

        Assert.assertNotNull(listResult)
        Assert.assertNull(placeListViewModel?.errorMessage)

    }

    @Test
    fun testSingleData() {
        val place = Place("", "Best Bar", "", "", Geometry(Location(0.3, 0.2)))
        val placeResponse = PlaceResponse(results = listOf(place))

        Mockito.`when`(placeApi.getPlaces("0,5", "bar", "distance", BuildConfig.API_KEY)).thenReturn(Observable.just(placeResponse))
        val result = placeApi.getPlaces("0,5", "bar", "distance", BuildConfig.API_KEY)

        val testObserver = TestObserver<PlaceResponse>()
        result.subscribe(testObserver)
        testObserver.assertComplete()
        testObserver.assertNoErrors()
        val listResult = testObserver.values()[0]

        Assert.assertEquals(listResult.results[0].name, "Best Bar")

    }

    @Test
    fun testSingleDataWithErrors() {
        val place = Place("", "Best Bar", "", "", Geometry(Location(0.3, 0.2)))
        val placeResponse = PlaceResponse(results = listOf(place))

        Mockito.`when`(placeApi.getPlaces("0,5", "bar", "distance", BuildConfig.API_KEY)).thenReturn(Observable.just(placeResponse))
        val result = placeApi.getPlaces("0,5", "bar", "distance", BuildConfig.API_KEY)

        val testObserver = TestObserver<PlaceResponse>()
        result.subscribe(testObserver)
        testObserver.assertComplete()
        testObserver.assertError(Throwable("OMG! This is an exception!"))

        Assert.assertNotNull(placeListViewModel?.errorMessage)

    }
}