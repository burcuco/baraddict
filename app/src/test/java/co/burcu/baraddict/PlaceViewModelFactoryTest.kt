package co.burcu.baraddict

import android.support.test.runner.AndroidJUnit4
import co.burcu.baraddict.ui.place.PlaceListFactoryViewModel
import co.burcu.baraddict.ui.place.PlaceListViewModel
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith

/**
 * Created by Burcu Yalcinkaya on 08/10/2018.
 */

@RunWith(AndroidJUnit4::class)
class PlaceViewModelFactoryTest {

    @Test
    fun testViewModelFactory() {
        val placeListFactoryViewModel = PlaceListFactoryViewModel("")
        val placeListViewModel = placeListFactoryViewModel.create(PlaceListViewModel::class.java)
        Assert.assertNotNull(placeListViewModel)
    }

}