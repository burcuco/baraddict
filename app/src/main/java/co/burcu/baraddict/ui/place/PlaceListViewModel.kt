package co.burcu.baraddict.ui.place

import android.arch.lifecycle.MutableLiveData
import android.view.View
import co.burcu.baraddict.BuildConfig.API_KEY
import co.burcu.baraddict.R
import co.burcu.baraddict.base.BaseViewModel
import co.burcu.baraddict.model.Place
import co.burcu.baraddict.model.PlaceResponse
import co.burcu.baraddict.network.PlaceApi
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

/**
 * Created by Burcu Yalcinkaya on 07/10/2018.
 */

class PlaceListViewModel(location: String) : BaseViewModel() {

    @Inject
    lateinit var placeApi: PlaceApi

    private lateinit var subscription: Disposable

    val loadingVisibility: MutableLiveData<Int> = MutableLiveData()
    val errorMessage: MutableLiveData<Int> = MutableLiveData()
    var errorClickListener = View.OnClickListener { loadPlaces() }
    val placeListAdapter: PlaceListAdapter = PlaceListAdapter()

    var placeResponse: List<Place>? = null

    var currentLocation: String = ""

    fun getPlaceData(location: String) {
        currentLocation = location
        if (placeResponse == null)
            loadPlaces()
    }

    override fun onCleared() {
        super.onCleared()
        subscription.dispose()
    }

    private fun loadPlaces() {
        subscription = placeApi.getPlaces(currentLocation, "bar", "distance", API_KEY)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { onRetrieveLaunchListStart() }
                .doOnTerminate { onRetrieveLaunchListFinish() }
                .subscribe(
                        { result -> onRetrieveLaunchListSuccess(result) },
                        { onRetrieveRocketListError() }
                )
    }

    private fun onRetrieveLaunchListStart() {
        loadingVisibility.value = View.VISIBLE
        errorMessage.value = null
    }

    private fun onRetrieveLaunchListFinish() {
        loadingVisibility.value = View.GONE
    }

    private fun onRetrieveLaunchListSuccess(list: PlaceResponse) {
        placeResponse = list.results
        placeListAdapter.updateLaunchList(list.results)
    }

    private fun onRetrieveRocketListError() {
       errorMessage.value = R.string.post_error
    }
}
