package co.burcu.baraddict.ui

import android.Manifest
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Bundle
import android.provider.Settings
import android.support.design.widget.BottomNavigationView
import android.support.v4.app.ActivityCompat
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.content.ContextCompat
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import co.burcu.baraddict.R
import co.burcu.baraddict.ui.place.PlaceListViewModel
import co.burcu.baraddict.util.BottomNavigationPosition
import co.burcu.baraddict.util.createFragment
import co.burcu.baraddict.util.extension.attach
import co.burcu.baraddict.util.extension.detach
import co.burcu.baraddict.util.findNavigationPositionById
import co.burcu.baraddict.util.getTag
import kotlinx.android.synthetic.main.activity_main.*
import co.burcu.baraddict.ui.place.PlaceListFactoryViewModel


class MainActivity : AppCompatActivity(), BottomNavigationView.OnNavigationItemSelectedListener {
    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        navPosition = findNavigationPositionById(item.itemId)
        return switchFragment(navPosition)
    }

    private var navPosition: BottomNavigationPosition = BottomNavigationPosition.PLACE
    private lateinit var bottomNavigation: BottomNavigationView
    private val requestFineLocation = 1
    private var locationListener: LocationListener? = null
    private var locationManager: LocationManager? = null
    private lateinit var alertDialog: AlertDialog
    private lateinit var model: PlaceListViewModel
    private var savedInstanceState: Bundle? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        bottomNavigation = findViewById(R.id.navigation)
        this.savedInstanceState = savedInstanceState
        navigation.setOnNavigationItemSelectedListener(this)
        alertDialog = getAlertDialog()

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            requestFineLocationPermission()
        }

    }

    override fun onPause() {
        super.onPause()
        locationManager?.removeUpdates(locationListener)
        locationListener = null
        locationManager = null
    }

    override fun onResume() {
        super.onResume()
        requestLocationUpdates(0, 400f)
    }

    private fun switchFragment(navPosition: BottomNavigationPosition): Boolean {
        return supportFragmentManager.findFragment(navPosition).let {
            if (it.isAdded) return false
            supportFragmentManager.detach() // Extension function
            supportFragmentManager.attach(it, navPosition.getTag()) // Extension function
            supportFragmentManager.executePendingTransactions()
        }
    }

    private fun initFragment(savedInstanceState: Bundle?) {
        savedInstanceState ?: switchFragment(BottomNavigationPosition.PLACE)
    }

    private fun FragmentManager.findFragment(position: BottomNavigationPosition): Fragment {
        return findFragmentByTag(position.getTag()) ?: position.createFragment()
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            requestFineLocation -> {
                if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    requestLocationUpdates(0, 400f)
                }
                return
            }
            else -> {
            }
        }
    }

    private fun requestFineLocationPermission() {
        ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), requestFineLocation)
    }

    private fun requestLocationUpdates(minTime: Long, minDistance: Float) {
        createLocationManager()
        createLocationListener()
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            if (checkIfLocationServicesEnabled()) {
                locationManager?.removeUpdates(locationListener)
                locationManager?.requestLocationUpdates(LocationManager.GPS_PROVIDER, minTime, minDistance, locationListener)
            } else {
                if (!alertDialog.isShowing) {
                    alertDialog.show()
                }
            }
        }
    }

    private fun checkIfLocationServicesEnabled(): Boolean {
        val locationSetting = Settings.Secure.getInt(contentResolver, Settings.Secure.LOCATION_MODE)
        if (locationSetting == 0) {
            return false
        }
        return true
    }

    private fun getAlertDialog(): AlertDialog {
        val builder = AlertDialog.Builder(this)
        builder.setTitle(getString(R.string.title_disabled_location_service))
        builder.setMessage(getString(R.string.message_enable_location_service))
        builder.setPositiveButton(getString(R.string.enable)) { dialog, which ->
            val gpsIntent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
            startActivity(gpsIntent)
        }
        builder.setNegativeButton(getString(R.string.exit)) { dialog, which ->
            dialog.dismiss()
            finish()
        }
        return builder.create()
    }

    private fun createLocationListener() {
        if (locationListener == null) {
            locationListener = object : LocationListener {
                override fun onLocationChanged(location: Location) { // Called when a new location is found by the network location provider.
                    val currentLocation = location.latitude.toString() + "," + location.longitude.toString()
                    val myViewModel = ViewModelProviders.of(this@MainActivity, PlaceListFactoryViewModel(currentLocation)).get(PlaceListViewModel::class.java)
                    myViewModel.getPlaceData(currentLocation)
                    model = myViewModel
                    initFragment(savedInstanceState)
                }

                override fun onStatusChanged(provider: String, status: Int, extras: Bundle) {}
                override fun onProviderEnabled(provider: String) {}
                override fun onProviderDisabled(provider: String) {}
            }
        }
    }

    private fun createLocationManager() {
        if (locationManager == null) {
            locationManager = this.getSystemService(Context.LOCATION_SERVICE) as LocationManager
        }
    }

}