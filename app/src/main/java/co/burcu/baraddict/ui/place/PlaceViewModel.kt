package co.burcu.baraddict.ui.place

import android.arch.lifecycle.MutableLiveData
import co.burcu.baraddict.base.BaseViewModel
import co.burcu.baraddict.model.Place

/**
 * Created by Burcu Yalcinkaya on 07/10/2018.
 */

class PlaceViewModel : BaseViewModel() {
    private var placeName = MutableLiveData<String>()
    private var placeId = MutableLiveData<Int>()
    private var placeVicinity = MutableLiveData<String>()
    private var placeIcon = MutableLiveData<String>()

    fun bind(place: Place) {
        this.placeName.value = place.name
        this.placeVicinity.value = place.vicinity
        this.placeIcon.value = place.icon
    }

    fun getPlaceName(): MutableLiveData<String> {
        return placeName
    }

    fun getPlaceVicinity(): MutableLiveData<String> {
        return placeVicinity
    }

    fun getPlaceImageUrl(): MutableLiveData<String> {
        return placeIcon
    }
}