package co.burcu.baraddict.ui.map

import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import co.burcu.baraddict.R
import co.burcu.baraddict.model.Place
import co.burcu.baraddict.ui.place.PlaceListFactoryViewModel
import co.burcu.baraddict.ui.place.PlaceListViewModel
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions


/**
 * Created by Burcu Yalcinkaya on 07/10/2018.
 */

class MapFragment : Fragment(), OnMapReadyCallback {
    private var mMap: GoogleMap? = null
    private lateinit var model: PlaceListViewModel

    override fun onMapReady(googleMap: GoogleMap?) {
        mMap = googleMap
        if (model.placeResponse != null) {
            for (item: Place in model.placeResponse!!) {
                val uca = LatLng(item.geometry.location.lat, item.geometry.location.lng)
                mMap?.addMarker(MarkerOptions().position(uca).title(item.name))?.showInfoWindow()
                mMap?.animateCamera(CameraUpdateFactory.newLatLngZoom(uca, 17f))
            }
        }
    }

    companion object {
        val TAG: String = MapFragment::class.java.simpleName
        fun newInstance() = MapFragment()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        activity?.title = getString(R.string.title_dashboard)
        model = activity?.run {
            ViewModelProviders.of(this, PlaceListFactoryViewModel("")).get(PlaceListViewModel::class.java)
        } ?: throw Exception("Invalid Activity")
        val view: View = inflater.inflate(R.layout.fragment_map, container, false)
        val mapFragment = childFragmentManager.findFragmentById(R.id.main_branch_map) as SupportMapFragment?
        mapFragment?.getMapAsync(this)

        return view
    }

}