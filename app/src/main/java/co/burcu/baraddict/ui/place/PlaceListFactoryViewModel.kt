package co.burcu.baraddict.ui.place

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider


/**
 * Created by Burcu Yalcinkaya on 08/10/2018.
 */

class PlaceListFactoryViewModel(private val mParam: String) : ViewModelProvider.NewInstanceFactory() {


    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return PlaceListViewModel(mParam) as T
    }
}