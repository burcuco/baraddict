package co.burcu.baraddict.ui.place

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.annotation.StringRes
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import co.burcu.baraddict.R
import co.burcu.baraddict.databinding.FragmentPlaceBinding
import android.content.Intent
import android.net.Uri
import co.burcu.baraddict.model.Place

/**
 * Created by Burcu Yalcinkaya on 07/10/2018.
 */

class PlaceFragment : Fragment() {
    private lateinit var binding: FragmentPlaceBinding
    private lateinit var viewModel: PlaceListViewModel
    private var errorSnackbar: Snackbar? = null

    companion object {
        val TAG: String = PlaceFragment::class.java.simpleName
        fun newInstance() = PlaceFragment()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        activity?.title = getString(R.string.title_home)
        var binding: FragmentPlaceBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_place, container, false)
        binding.placeList.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        viewModel = activity?.run {
            ViewModelProviders.of(this, PlaceListFactoryViewModel("")).get(PlaceListViewModel::class.java)

        } ?: throw Exception(getString(R.string.error_invalid_activity))
        viewModel.errorMessage.observe(this, Observer { errorMessage ->
            if (errorMessage != null) showError(errorMessage) else hideError()

        })
        binding.viewModel = viewModel
        viewModel.placeListAdapter.onItemClick = {
            openCurrentMaps(it)
        }
        return binding.root

    }

    private fun openCurrentMaps(place: Place) {
        val uri: String = getString(R.string.geo_uri) + place.name + getString(R.string.geo_uri_place) + place.id
        val gmmIntentUri = Uri.parse(uri)
        val mapIntent = Intent(Intent.ACTION_VIEW, gmmIntentUri)
        mapIntent.setPackage(getString(R.string.open_maps_package))
        startActivity(mapIntent)
    }

    private fun hideError() {
        errorSnackbar?.dismiss()
    }

    private fun showError(@StringRes errorMessage: Int) {
        errorSnackbar = Snackbar.make(binding.root, errorMessage, Snackbar.LENGTH_INDEFINITE)
        errorSnackbar?.setAction(R.string.retry, viewModel.errorClickListener)
        errorSnackbar?.show()
    }
}