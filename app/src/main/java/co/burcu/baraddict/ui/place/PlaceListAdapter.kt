package co.burcu.baraddict.ui.place

import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import co.burcu.baraddict.R
import co.burcu.baraddict.databinding.ItemPlaceBinding
import co.burcu.baraddict.model.Place

/**
 * Created by Burcu Yalcinkaya on 07/10/2018.
 */

class PlaceListAdapter : RecyclerView.Adapter<PlaceListAdapter.ViewHolder>() {
    private lateinit var placeList: List<Place>
    var onItemClick: ((Place) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PlaceListAdapter.ViewHolder {
        val binding: ItemPlaceBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.item_place, parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: PlaceListAdapter.ViewHolder, position: Int) {
        holder.bind(placeList[position])
    }

    override fun getItemCount(): Int {
        return if (::placeList.isInitialized) placeList.size else 0
    }

    fun updateLaunchList(launchList: List<Place>) {
        this.placeList = launchList
        notifyDataSetChanged()
    }

    inner class ViewHolder(private val binding: ItemPlaceBinding) : RecyclerView.ViewHolder(binding.root) {
        private val viewModel = PlaceViewModel()

        fun bind(launch: Place) {
            viewModel.bind(launch)
            binding.viewModel = viewModel

            itemView.setOnClickListener {
                onItemClick?.invoke(launch)
            }
        }

    }
}