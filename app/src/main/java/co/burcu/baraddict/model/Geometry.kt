package co.burcu.baraddict.model

/**
 * Created by Burcu Yalcinkaya on 07/10/2018.
 */

data class Geometry(val location: Location)