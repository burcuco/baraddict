package co.burcu.baraddict.model

/**
 * Created by Burcu Yalcinkaya on 07/10/2018.
 */

data class Place(val id: String, val name: String, val icon: String, val vicinity: String, val geometry: Geometry)