package co.burcu.baraddict.model

import java.io.Serializable

/**
 * Created by Burcu Yalcinkaya on 07/10/2018.
 */

data class PlaceResponse(val results: List<Place>) : Serializable