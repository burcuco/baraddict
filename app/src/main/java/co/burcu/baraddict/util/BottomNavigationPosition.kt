package co.burcu.baraddict.util

import android.support.v4.app.Fragment
import co.burcu.baraddict.R
import co.burcu.baraddict.ui.map.MapFragment
import co.burcu.baraddict.ui.place.PlaceFragment

/**
 * Created by Burcu Yalcinkaya on 07/10/2018.
 */

enum class BottomNavigationPosition(val position: Int, val id: Int) {
    PLACE(0, R.id.navigation_home),
    MAP(1, R.id.navigation_dashboard),
}

fun findNavigationPositionById(id: Int): BottomNavigationPosition = when (id) {
    BottomNavigationPosition.PLACE.id -> BottomNavigationPosition.PLACE
    BottomNavigationPosition.MAP.id -> BottomNavigationPosition.MAP
    else -> BottomNavigationPosition.PLACE
}

fun BottomNavigationPosition.createFragment(): Fragment = when (this) {
    BottomNavigationPosition.PLACE -> PlaceFragment.newInstance()
    BottomNavigationPosition.MAP -> MapFragment.newInstance()
}

fun BottomNavigationPosition.getTag(): String = when (this) {
    BottomNavigationPosition.PLACE -> PlaceFragment.TAG
    BottomNavigationPosition.MAP -> MapFragment.TAG
}
