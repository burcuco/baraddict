package co.burcu.baraddict.network

/**
 * Created by Burcu Yalcinkaya on 07/10/2018.
 */

import co.burcu.baraddict.model.PlaceResponse
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query

interface PlaceApi {

    @GET("maps/api/place/nearbysearch/json?")
    fun getPlaces(
            @Query("location") location: String,
            @Query("type") type: String,
            @Query("rankby") rankby: String,
            @Query("key") key: String): Observable<PlaceResponse>


}