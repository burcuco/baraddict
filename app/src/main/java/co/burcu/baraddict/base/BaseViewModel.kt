package co.burcu.baraddict.base

import android.arch.lifecycle.ViewModel
import co.burcu.baraddict.injection.component.DaggerViewModelInjector
import co.burcu.baraddict.injection.component.ViewModelInjector
import co.burcu.baraddict.injection.module.NetworkModule
import co.burcu.baraddict.ui.place.PlaceListViewModel

/**
 * Created by Burcu Yalcinkaya on 07/10/2018.
 */

abstract class BaseViewModel : ViewModel() {
    private val injector: ViewModelInjector = DaggerViewModelInjector
            .builder()
            .networkModule(NetworkModule)
            .build()

    init {
        inject()
    }

    /**
     * Injects the required dependencies
     */
    private fun inject() {
        when (this) {
            is PlaceListViewModel -> injector.inject(this)
        }
    }

}