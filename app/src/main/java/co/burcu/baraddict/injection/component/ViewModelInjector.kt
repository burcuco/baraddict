package co.burcu.baraddict.injection.component

import co.burcu.baraddict.injection.module.NetworkModule
import co.burcu.baraddict.ui.place.PlaceListViewModel
import dagger.Component
import javax.inject.Singleton

/**
 * Created by Burcu Yalcinkaya on 07/10/2018.
 */

/**
 * Component providing inject() methods for presenters.
 */
@Singleton
@Component(modules = [(NetworkModule::class)])
interface ViewModelInjector {
    /**
     * Injects required dependencies into the specified LaunchListViewModel.
     * @param placeListViewModel LaunchListViewModel in which to inject the dependencies
     */
    fun inject(placeListViewModel: PlaceListViewModel)

    @Component.Builder
    interface Builder {
        fun build(): ViewModelInjector

        fun networkModule(networkModule: NetworkModule): Builder
    }
}