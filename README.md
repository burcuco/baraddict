# README #

### What is this repository for? ###

* Tide Interview Task
* 1.0

## Technologies ##

### Android ###
* Kotlin
* MVVM
* Retrofit
* Dagger2
* Rx Java
* Binding
* Glide
* Google Maps
* Google Places
* Android Lifecycle
* Android Support Libraries

### Unit Tests ###
* Android JUnit4
* Mockito Android
* Mockito Core

### Contact Info ###
* me@burcu.co 
* burcuuturkmen@gmail.com

Thank you!
